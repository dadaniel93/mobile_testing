package plugin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;

import eyeserver.AppState;

public class SerializeState
{
	/**
	 * Save the state tree to filePath
	 * @param stateTree
	 * @param filePath
	 * @return true if done
	 */
	public Boolean saveState(AppState stateTree, String filePath)
	{
		// Save state tree
		if(!saveObject(filePath, stateTree))
		{
			return false;
		}
		
		// Get weekday number
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		// Save a backup
		filePath+=dayOfWeek;
		if(!saveObject(filePath, stateTree))
		{
			return false;
		}

		return true;
	}

	/**
	 * Load state tree from filePath
	 * or create a new state if not found
	 * @param filePath
	 * @return A state tree
	 */
	public AppState loadState(String filePath)
	{
		AppState stateTree=null;
		Object object=loadObject(filePath);
		if(object!=null)
		{
			stateTree=(AppState)object;
		}
		else
		{
			// Create a new state
			stateTree=new AppState("0", "Home");
		}
		return stateTree;
	}

	private Object loadObject(String filepath)
	{
		try
		{
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			Object object = in.readObject();
			in.close();
			fileIn.close();
			return object;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private boolean saveObject(String filepath, Object object)
	{
		try
		{
			FileOutputStream fileOut = new FileOutputStream(filepath);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(object);
			out.flush();
			out.close();
			fileOut.close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
}
