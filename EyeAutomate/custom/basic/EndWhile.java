package custom.basic;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Properties;

import eye.Match;
import eyeautomate.Command;
import eyeautomate.CustomCommand;

public class EndWhile extends EyeCommand
{
	private static final String ICON_FILENAME="icons/while2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		Command blockCommand=scriptRunner.getBlockCommand();
		if(blockCommand!=null && scriptRunner.getCommandLevel()==1)
		{
			if(blockCommand instanceof CustomCommand)
			{
				scriptRunner.decreaseCommandLevel();
				CustomCommand customCommand=(CustomCommand)blockCommand;
				String command=customCommand.getCommand();
				String[] commandLine=customCommand.getCommandParameters();
				String param=commandLine[0];
				for (int iterationNo = 0; iterationNo < scriptRunner.getIterations(); iterationNo++)
				{
					if (scriptRunner.isStop())
					{
						// Continue with next operation
						return false;
					}

					if(scriptRunner.isExpression(param))
					{
						// An expression
						boolean passed=command.equalsIgnoreCase("While")?scriptRunner.evaluate(param):!scriptRunner.evaluate(param);
						if(!passed)
						{
							scriptRunner.decreaseCommandLevel();
							return true;
						}
						if(!callChildScript(customCommand, passed, iterationNo+1, scriptParameters))
						{
							return false;
						}
					}
					else
					{
						// An image
						BufferedImage imageToFind = scriptRunner.loadImage(param);
						if (imageToFind == null)
						{
							scriptParameters.put("Error", "Image/Text not found");
							return false;
						}
						BufferedImage capture = eye.createScreenCapture();
						if(capture==null)
						{
							scriptParameters.put("Error", "Out of memory. Increase Java heap space");
							return false;
						}

						Rectangle targetArea=null;
						if (commandLine.length == 5)
						{
							int x = string2Int(commandLine[1]);
							int y = string2Int(commandLine[2]);
							int width = string2Int(commandLine[3]);
							int height = string2Int(commandLine[4]);
							targetArea=new Rectangle(x, y, width, height);
						}

						Match match=eye.findImage(capture, imageToFind, targetArea);
						boolean passed=command.equalsIgnoreCase("While")?match!=null:match==null;
						if(!passed)
						{
							scriptRunner.decreaseCommandLevel();
							return true;
						}
						if(!callChildScript(customCommand, passed, iterationNo+1, scriptParameters))
						{
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private boolean callChildScript(CustomCommand customCommand, boolean passed, int iterationNo, Properties scriptParameters)
	{
		int lineNo=scriptRunner.getCurrentLineNo();
		int noScriptLines=customCommand.getNoScriptLines();
		if(passed)
		{
			if(!scriptRunner.callChildScript(lineNo-noScriptLines-1, iterationNo, customCommand.getScript(), scriptParameters))
			{
				scriptParameters.putAll(scriptRunner.getParameters());
				return false;
			}
		}
		scriptParameters.putAll(scriptRunner.getParameters());
		return true;
	}
	
	public String getTooltip()
	{
		return null;
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Iteration"};
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}
}
