package custom.basic;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class KeyRecorderDialog extends JDialog implements KeyListener
{
	private static final long serialVersionUID = -8310257023672065320L;
	private static final int WIDTH = 400;
	private static final int HEIGHT = 300;

	private JTextArea messageText=null;
	private String returnedMessageText=null;
	private Stack<Integer> pressedKeys=new Stack<Integer>();

	public KeyRecorderDialog()
	{
		createDialog(WIDTH, HEIGHT);
	}

	private void createDialog(int width, int height)
	{
		// Center dialog on parent
		setSize(width, height);
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Key Recorder");
		setModal(true);
		
		setLayout(new BorderLayout());

		messageText = createTextArea();
		JScrollPane sp = new JScrollPane(messageText);
		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		messageText.addKeyListener(this);
		messageText.requestFocus();
		messageText.setFocusTraversalKeysEnabled(false);

		getContentPane().add(sp, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel();
		JButton clearButton = new JButton("Clear");
		clearButton.setFocusable(false);
		buttonPanel.add(clearButton);
		JButton insertButton = new JButton("Insert");
		insertButton.setFocusable(false);
		buttonPanel.add(insertButton);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setFocusable(false);
		buttonPanel.add(cancelButton);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		clearButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				messageText.setText("");
				messageText.requestFocus();
			}
		});

		insertButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				returnedMessageText=messageText.getText();
				setVisible(false);
			}
		});

		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				setVisible(false);
			}
		});
	}

	private JTextArea createTextArea()
	{
		JTextArea t = new JTextArea();
		t.setLineWrap(true);
		t.setWrapStyleWord(true);
		t.setFont(this.getFont());
		t.setEditable(false);
		return t;
	}

	public void keyTyped(KeyEvent e)
	{
	}

	public void keyPressed(KeyEvent e)
	{
		int extendedKeyCode=e.getExtendedKeyCode();
		char keyChar=e.getKeyChar();
		if(pressedKeys.isEmpty() || !sameKeyCode(pressedKeys.peek(), extendedKeyCode))
		{
			pressedKeys.push(extendedKeyCode);
			String typedText=getKeyText(false, extendedKeyCode, keyChar);
			if(typedText.length()>0)
			{
				messageText.setText(messageText.getText()+typedText);
			}
		}
	}

	public void keyReleased(KeyEvent e)
	{
		int extendedKeyCode=e.getExtendedKeyCode();
		char keyChar=e.getKeyChar();
		if(!pressedKeys.isEmpty() && sameKeyCode(pressedKeys.peek(), extendedKeyCode))
		{
			// Same key
			pressedKeys.pop();
			String typedText=getKeyText(true, extendedKeyCode, keyChar);
			if(typedText.length()>0)
			{
				messageText.setText(messageText.getText()+typedText);
			}
		}
	}

	private boolean sameKeyCode(int keyCode1, int keyCode2)
	{
		if(keyCode1==keyCode2)
		{
			return true;
		}
		return false;
	}

	/**
	 * @return The text entered
	 */
	public String getText()
	{
		return returnedMessageText;
	}

	private static String getKeyText(boolean keyUp, int keyCode, char keyChar)
	{
		if(keyUp)
		{
			// Key up
			switch (keyCode)
			{
				case KeyEvent.VK_SHIFT:
				case KeyEvent.VK_CONTROL:
				case KeyEvent.VK_ALT:
				case KeyEvent.VK_ALT_GRAPH:
					return "";
				case KeyEvent.VK_WINDOWS:
					return "[WINDOWS_RELEASE]";
				case KeyEvent.VK_META:
					return "[META_RELEASE]";
				default:
					return "";
			}
		}
		else
		{
			// Key down
			if (keyCode >= KeyEvent.VK_NUMPAD0 && keyCode <= KeyEvent.VK_NUMPAD9)
			{
				char c = (char) (keyCode - KeyEvent.VK_NUMPAD0 + '0');
				return "[NUMPAD" + c + "]";
			}

			switch (keyCode)
			{
				case KeyEvent.VK_SHIFT:
				case KeyEvent.VK_CONTROL:
				case KeyEvent.VK_ALT:
				case KeyEvent.VK_ALT_GRAPH:
					return "";
				case KeyEvent.VK_WINDOWS:
					return "[WINDOWS_PRESS]";
				case KeyEvent.VK_META:
					return "[META_PRESS]";
				case KeyEvent.VK_SPACE:
					return " ";
				case KeyEvent.VK_DELETE:
					return "[DELETE]";
				case KeyEvent.VK_HOME:
					return "[HOME]";
				case KeyEvent.VK_END:
					return "[END]";
				case KeyEvent.VK_ENTER:
					return "[ENTER]";
				case KeyEvent.VK_BACK_SPACE:
					return "[BACKSPACE]";
				case KeyEvent.VK_TAB:
					return "[TAB]";
				case KeyEvent.VK_ESCAPE:
					return "[ESCAPE]";
				case KeyEvent.VK_INSERT:
					return "[INSERT]";
				case KeyEvent.VK_PAGE_UP:
					return "[PAGE_UP]";
				case KeyEvent.VK_PAGE_DOWN:
					return "[PAGE_DOWN]";
				case KeyEvent.VK_F1:
					return "[F1]";
				case KeyEvent.VK_F2:
					return "[F2]";
				case KeyEvent.VK_F3:
					return "[F3]";
				case KeyEvent.VK_F4:
					return "[F4]";
				case KeyEvent.VK_F5:
					return "[F5]";
				case KeyEvent.VK_F6:
					return "[F6]";
				case KeyEvent.VK_F7:
					return "[F7]";
				case KeyEvent.VK_F8:
					return "[F8]";
				case KeyEvent.VK_F9:
					return "[F9]";
				case KeyEvent.VK_F10:
					return "[F10]";
				case KeyEvent.VK_F11:
					return "[F11]";
				case KeyEvent.VK_F12:
					return "[F12]";
				case KeyEvent.VK_LEFT:
					return "[LEFT]";
				case KeyEvent.VK_UP:
					return "[UP]";
				case KeyEvent.VK_RIGHT:
					return "[RIGHT]";
				case KeyEvent.VK_DOWN:
					return "[DOWN]";
				default:
					if('\"'==keyChar)
						return "\\\"";
					else if('\\'==keyChar)
						return "\\\\";
					else
						return ""+keyChar;
			}
		}
	}
}
