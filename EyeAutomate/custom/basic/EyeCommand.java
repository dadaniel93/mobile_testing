package custom.basic;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import eye.Eye;
import eyeautomate.ScriptRunner;

public class EyeCommand
{
	protected static final String LICENSE_PROPERTIES_FILE = "settings/license.properties";

	protected ScriptRunner scriptRunner=null;
	protected Eye eye=null;

	/**
	 * Get the currently used instance of Eye or create a new one.
	 */
	protected void getEye()
	{
		eye=(Eye)scriptRunner.getRecognitionEngine();
		if(eye==null)
		{
			eye=new Eye();
			scriptRunner.setRecognitionEngine(eye);
		}
		WebDriver webDriver=(WebDriver)scriptRunner.getWebDriver();
		if(isBrowserOpen(webDriver))
		{
			String licenseKey=loadLicenseKey();
			if(licenseKey!=null)
			{
				eye.setWebDriver(webDriver, licenseKey);
			}
		}
		else
		{
			scriptRunner.setWebDriver(null);
			eye.removeWebDriver();
		}
	}

	private boolean isBrowserOpen(WebDriver webDriver)
	{
		if(webDriver==null)
		{
			return false;
		}
		try
		{
			webDriver.getCurrentUrl();
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public void stopScript()
	{
		eye.setStop(true);
	}

	public void startScript()
	{
		eye.resetImageRecognitionSettings();
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		getEye();
	}

	private String loadLicenseKey()
	{
		try
		{
			Properties licenseProperties = new Properties();
			FileInputStream in = new FileInputStream(LICENSE_PROPERTIES_FILE);
			licenseProperties.load(in);
			in.close();
			return licenseProperties.getProperty("eyesel_license_key");
		}
		catch (Exception e)
		{
			return null;
		}
	}
}
