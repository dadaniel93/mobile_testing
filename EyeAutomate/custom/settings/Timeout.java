package custom.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;

public class Timeout extends EyeCommand
{
	private static final String COMMAND="Timeout";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Time to wait for an image to appear on screen before failing<br/>Usage: <br/>"+COMMAND+" Seconds</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter timeout in seconds>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Timeout command. Usage: Timeout Seconds", lineNo);
			return false;
		}
		else
		{
			String timeoutStr = commandLine.get(1);
			if (isIntString(timeoutStr))
			{
				int timeoutSeconds = string2Int(timeoutStr);
				if (timeoutSeconds < 1 || timeoutSeconds > 1000000)
				{
					errorLogMessage(scriptFilename, "Specify a Timeout between 1 and 1000000", lineNo);
					return false;
				}
				else
				{
					eye.setTimeout(timeoutSeconds);
					scriptRunner.getVizionEngine().setTimeout(timeoutSeconds);
				}
			}
			else
			{
				errorLogMessage(scriptFilename, "Specify a Timeout between 1 and 1000000", lineNo);
				return false;
			}
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
