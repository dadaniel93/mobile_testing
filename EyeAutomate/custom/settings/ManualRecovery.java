package custom.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class ManualRecovery
{
	private static final String COMMAND="ManualRecovery";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Recover from failed commands using a manual test instruction<br/>Usage: <br/>"+COMMAND+" Yes/No</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Confirm>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in ManualRecovery command. Usage: ManualRecovery Yes/No", lineNo);
			return false;
		}
		else
		{
			String param = commandLine.get(1);
			if (param.equalsIgnoreCase("no"))
			{
				scriptRunner.setManualRecovery(false);
			}
			else if (param.equalsIgnoreCase("yes"))
			{
				scriptRunner.setManualRecovery(true);
			}
			else
			{
				errorLogMessage(scriptFilename, "Invalid ManualRecovery option, specify Yes or No", lineNo);
				return false;
			}
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
