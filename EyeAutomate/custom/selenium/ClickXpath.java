package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import eyeautomate.ScriptRunner;

public class ClickXpath
{
	private static final String COMMAND="ClickXpath";
	private static final String ICON_FILENAME="icons/mouse2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: ClickXpath Xpath");
			return false;
		}

		String xpath=commandParameters[0].trim();
		int dX = Integer.parseInt(commandParameters[1].trim());
		int dY = Integer.parseInt(commandParameters[2].trim());


		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebDriverWait wait = new WebDriverWait(webDriver, scriptRunner.getVizionEngine().getTimeout());
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
//			element.click();
			int height=element.getSize().getHeight();
			int width=element.getSize().getWidth();
			int x=element.getLocation().getX();
			int y=element.getLocation().getY();
			new Actions(webDriver).moveToElement(element).moveByOffset(-(x+width/2)+dX, -(y+height/2)+dY).click().perform();
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter XPath>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Clicks on a widget identified by an XPath<br/>Usage:<br/>"+COMMAND+" XPath</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
