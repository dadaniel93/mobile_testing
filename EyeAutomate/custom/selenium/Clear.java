package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import eyeautomate.ScriptRunner;

public class Clear
{
	private static final String COMMAND="Clear";
	private static final String ICON_FILENAME="icons/delete2.png";
	private WebDriver webDriver=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebElement currentElement = webDriver.switchTo().activeElement();
			if(currentElement!=null)
			{
				currentElement.clear();
				return true;
			}
			else
			{
				scriptParameters.put("Error", "No active element");
				return false;
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Clears the text field in focus<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
