package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import eyeautomate.ScriptRunner;

public class CloseBrowser
{
	private static final String COMMAND="CloseBrowser";
	private static final String ICON_FILENAME="icons/stop2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			webDriver.close();
			webDriver.quit();
			webDriver=null;
			scriptRunner.setWebDriver(webDriver);
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Closes the browser<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
