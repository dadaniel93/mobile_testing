package custom.extended;

import java.util.Properties;

import eyeautomate.HttpServiceCaller;

public class RunRemote implements Runnable
{
	private static final String COMMAND="RunRemote";
	private static final String ICON_FILENAME="icons/call2.png";
	private String address=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" HttpRequest");
			return false;
		}
		address=commandParameters[0].trim();
		try
		{
			Thread thread = new Thread(this);
			thread.start();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	public String getTooltip()
	{
		return "Runs a remote script";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}
	
	public String getCommand()
	{
		return COMMAND+" \"<String: Enter HTTP request=http://localhost:1234>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void run()
	{
		HttpServiceCaller service=new HttpServiceCaller();
		service.executeGetRequest(address);
	}
}
