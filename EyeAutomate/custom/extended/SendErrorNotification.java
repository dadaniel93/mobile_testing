package custom.extended;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;



public class SendErrorNotification {


        private static final String COMMAND="SendEndNotification";
        private static final String ICON_FILENAME="icons/analysis2.png";


        public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
        {

            if(commandParameters.length<1)
            {
                scriptParameters.put("Error", "Missing parameter in command.");
                return false;
            }
            List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
            String ErrorMessage = commandLine.get(0);
            sendSlackMessage(ErrorMessage);

            return true;
        }


        public String getTooltip()
        {
            return "<html>Sending  Error  message to Slack br/>Usage: <br/>"+COMMAND+"</html>";

        }

        public String[] getParameters()
        {
            return new String[]{"Error"};
        }

        public String getIconFilename()
        {
            return ICON_FILENAME;
        }

        public String getHelp()
        {
            return "http://www.eyeautomate.com/extendedcommands.html";

        }
        public void sendSlackMessage(String ErrorMessage){

            String url = "https://hooks.slack.com/services/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";
            String message = "Start";

            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httppost = new HttpPost(url);


            try {
                httppost.addHeader("Content-type", "application/json");

                StringEntity params = new StringEntity("{\"attachments\" : [ {\"fallback\":\"" + ErrorMessage +  "\"," +
                        "\"pretext\" : \"" + ErrorMessage +  "\",\"color\" : \"FA0A0A\", " +
                        "\"fields\":[ {\"title\" : \"Status: Error\" }] }] }","UTF-8");

                params.setContentType("application/json");
                httppost.setEntity(params);

                HttpResponse response = client.execute(httppost);
                System.out.println(response.getStatusLine().getStatusCode());

            } catch(

                    IOException ie) {
                ie.printStackTrace();
            }

        }


    }





