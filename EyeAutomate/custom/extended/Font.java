package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;
import eyeautomate.VizionEngine;

public class Font
{
	private static final String COMMAND="Font";
	private static final String ICON_FILENAME="icons/font2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Sets the current font<br/>Usage: <br/>"+COMMAND+" Font Name Size [Plain|Bold|Italic] [Kerning] [Underline]</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "FontName", "FontSize", "FontStyle"};
	}

	public String getCommand()
	{
		return COMMAND+" <Font>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private VizionEngine getVizionEngine()
	{
		return scriptRunner.getVizionEngine();
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 3)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Font command. Usage: Font Name Size [Plain|Bold|Italic] [Kerning] [Underline]", lineNo);
			return false;
		}
		else
		{
			String fontName = commandLine.get(1);
			String fontSize = commandLine.get(2);
			String fontStyle = "Plain";
			if(commandLine.size()>3)
			{
				fontStyle = commandLine.get(3);
			}

			if (!isIntString(fontSize))
			{
				errorLogMessage(scriptFilename, "Specify Size as an integer value", lineNo);
				return false;
			}

			if(fontName.length()==0)
			{
				errorLogMessage(scriptFilename, "Specify a font Name", lineNo);
				return false;
			}

			String currentFontName=fontName;
			int currentFontSize=string2Int(fontSize);
			int currentFontStyle=java.awt.Font.PLAIN;
			boolean currentFontKerning=false;
			boolean currentFontUnderline=false;
			
			for (int i = 3; i < commandLine.size(); i++)
			{
				fontStyle=commandLine.get(i);
				if (fontStyle.equalsIgnoreCase("plain"))
				{
					currentFontStyle|=java.awt.Font.PLAIN;
				}
				else if (fontStyle.equalsIgnoreCase("bold"))
				{
					currentFontStyle|=java.awt.Font.BOLD;
				}
				else if (fontStyle.equalsIgnoreCase("italic"))
				{
					currentFontStyle|=java.awt.Font.ITALIC;
				}
				else if (fontStyle.equalsIgnoreCase("kerning"))
				{
					currentFontKerning=true;
				}
				else if (fontStyle.equalsIgnoreCase("underline"))
				{
					currentFontUnderline=true;
				}
			}
			
			scriptParameters.setProperty("FontName", currentFontName);
			scriptParameters.setProperty("FontStyle", fontStyle);
			scriptParameters.setProperty("FontSize", fontSize);
			
			getVizionEngine().setCurrentFontName(currentFontName);
			getVizionEngine().setCurrentFontSize(currentFontSize);
			getVizionEngine().setCurrentFontStyle(currentFontStyle);
			getVizionEngine().setCurrentFontKerning(currentFontKerning);
			getVizionEngine().setCurrentFontUnderline(currentFontUnderline);
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
