package custom.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class Set
{
	private static final String COMMAND="Set";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Set the value of a parameter<br/>Usage: <br/>"+COMMAND+" Parameter Value<br/>"+COMMAND+" Parameter Value1 Value2 ...</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter parameter name>\" \"<String: Enter parameter value>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 3)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Set command. Usage: Set Parameter Value [Value...]", lineNo);
			return false;
		}
		else
		{
			if (commandLine.size() == 3)
			{
				String parameter = commandLine.get(1);
				String value = commandLine.get(2);
				scriptParameters.put(parameter, value);
			}
			else
			{
				// More than one value
				String parameter = commandLine.get(1);
				int count=commandLine.size()-2;
				for(int i=0; i<count; i++)
				{
					String value = commandLine.get(i+2);
					scriptParameters.put(parameter+(i+1), value);
				}
				scriptParameters.put(parameter+"Count", ""+count);
			}
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
