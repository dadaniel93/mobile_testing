package custom.extended;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebElement;

import custom.basic.EyeCommand;
import eye.Match;
import eyeautomate.Location;
import eyeautomate.Locations;
import eyeautomate.ScriptRunner;

public class DragDrop extends EyeCommand
{
	private static final String COMMAND="DragDrop";
	private static final String ICON_FILENAME="icons/mouse2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(passed)
		{
			scriptRunner.setScreenshot(eye.captureCurrentLocation());
		}
		else
		{
			scriptRunner.setScreenshot(eye.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Completes dragging to an image target<br/>Waits until the image appears<br/>Usage:<br/>"+COMMAND+" Image [Location/Area]<br/>"+COMMAND+" X Y<br/>"+COMMAND+" ImageX ImageY</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
	
	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Determine if s is an integer that begins with a - or + sign
	 * @param s
	 * @return true if s is an integer that begins with - or +
	 */
	private static boolean isRelativeIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char must contain - or +
				if (!(c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private BufferedImage loadImage(String filename)
	{
		return scriptRunner.loadImage(filename);
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in "+command+" command. Usage: "+command+" [Image/Text] [PercentX|RelativeX|ImageX] [PercentY|RelativeY|ImageY] [Width] [Height]", lineNo);
			return false;
		}
		else
		{
			if (commandLine.size() == 3)
			{
				// X and Y images or coordinates
				String x = commandLine.get(1);
				String y = commandLine.get(2);
				int nextX=0;
				int nextY=0;
				if(isRelativeIntString(x))
				{
					nextX=eye.getLastX()+string2Int(x);
				}
				else if (isIntString(x))
				{
					nextX = string2Int(x);
				}
				else
				{
					BufferedImage imageXToFind = loadImage(x);
					if (imageXToFind == null)
					{
						errorLogMessage(scriptFilename, "Image not found", lineNo);
						return false;
					}

					Match match=eye.findImage(imageXToFind);
					if(match==null)
					{
						errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
						return false;
					}

					Point centerX=match.getCenterLocation();
					nextX=(int)centerX.getX();
				}
				if(isRelativeIntString(y))
				{
					nextY=eye.getLastY()+string2Int(y);
				}
				else if (isIntString(y))
				{
					nextY = string2Int(y);
				}
				else
				{
					BufferedImage imageYToFind = loadImage(y);
					if (imageYToFind == null)
					{
						errorLogMessage(scriptFilename, "Image not found", lineNo);
						return false;
					}
					Match match=eye.findImage(imageYToFind);
					if(match==null)
					{
						errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
						return false;
					}

					Point centerY=match.getCenterLocation();
					nextY=(int)centerY.getY();
				}
				eye.dragDrop(new Point(nextX, nextY));
			}
			else if (commandLine.size() == 4)
			{
				// Image and relative position
				String filename = commandLine.get(1);
				String xs = commandLine.get(2);
				String ys = commandLine.get(3);

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				Match match=eye.findImage(imageToFind);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}

				eye.dragDrop(match.getRelativeLocation(xs, ys));
			}
			else if (commandLine.size() == 6)
			{
				String filename = commandLine.get(1);
				if(!(isIntString(commandLine.get(2)) && isIntString(commandLine.get(3)) && isIntString(commandLine.get(4)) && isIntString(commandLine.get(5))))
				{
					errorLogMessage(scriptFilename, "Specify coordinates and size using integer values", lineNo);
					return false;
				}

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				// Create a target area to check
				int x = string2Int(commandLine.get(2));
				int y = string2Int(commandLine.get(3));
				int width = string2Int(commandLine.get(4));
				int height = string2Int(commandLine.get(5));
				Rectangle targetArea=new Rectangle(x, y, width, height);

				Match match=eye.findImage(imageToFind, targetArea);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}

				// Click in the center of the box
				int relativePosX=x+width/2;
				int relativePosY=y+height/2;
				eye.dragDrop(match.getRelativeLocation(relativePosX, relativePosY));
			}
			else if (ScriptRunner.isWidget(commandLine.get(1)))
			{
				String widgetScriptPath = commandLine.get(1);
				if(!scriptRunner.runWidgetScript(widgetScriptPath))
				{
					scriptParameters.put("Error", "Failed to run Widget script");
					return false;
				}
				
				Locations locations=scriptRunner.getLocations();
				List<Location> sortedLocations=locations.getSortedLocations();
				
				for(Location location:sortedLocations)
				{
					Object unspecifiedElement=location.getElement();
					if(unspecifiedElement!=null && unspecifiedElement instanceof WebElement)
					{
						// A Selenium WebElement
						WebElement webElement=(WebElement)unspecifiedElement;
						webElement.click();
						return true;
					}
					else if(location.getPosition()!=null)
					{
						// Has a position
						Rectangle rect=location.getPosition();
						int x=(int)rect.getCenterX();
						int y=(int)rect.getCenterY();
						eye.dragDrop(new Point(x, y));
						return true;
					}
				}

				scriptParameters.put("Error", "Failed to locate Widget");
				return false;
			}
			else if (ScriptRunner.isImage(commandLine.get(1)))
			{
				// One image
				String filename = commandLine.get(1);
				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				Match match=eye.findImage(imageToFind);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				eye.dragDrop(match.getCenterLocation());
			}
			else
			{
				// One text
				String text = commandLine.get(1);
				String fontName=scriptParameters.getProperty("FontName", "Consolas");
				String fontStyleStr=scriptParameters.getProperty("FontStyle", "Plain");
				int fontStyle=Font.PLAIN;
				if (fontStyleStr.equalsIgnoreCase("bold"))
				{
					fontStyle=Font.BOLD;
				}
				else if (fontStyleStr.equalsIgnoreCase("italic"))
				{
					fontStyle=Font.ITALIC;
				}
				int fontSize=string2Int(scriptParameters.getProperty("FontSize", "11"));
				Font font=eye.createFont(fontName, fontStyle, fontSize, false, false);
				BufferedImage imageToFind = eye.createImageFromText(text, font, Color.BLACK, Color.WHITE);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Failed to create image", lineNo);
					return false;
				}

				Match match=eye.findImage(imageToFind);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, text not found", lineNo);
					return false;
				}
				eye.dragDrop(match.getCenterLocation());
			}

			return true;
		}
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
